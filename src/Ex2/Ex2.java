package Ex2;



public class Ex2 {


    static class MyThread implements Runnable {

        // run() method whenever thread is invoked
        public void run(){
            long start = System.currentTimeMillis();
            System.out.println("Current Thread Name- " + Thread.currentThread().getName());
            // Getting thread's ID
            System.out.println(", time : " + (start));
        }
        {

            try {

                Thread.sleep(2000);
            }

            catch (Exception err) {


                System.out.println(err);
            }
        }
    }

    public static void main(String[] args) {
        // Creating 3 threads, passing thread name as arg
        Thread t1 = new Thread(new MyThread(), "CThread1");
        Thread t2 = new Thread(new MyThread(), "Cthread2");
        for(int i=0;i<=7;i++) {
            t1.run();
            t2.run();
        }

    }
}



